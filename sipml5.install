<?php
/**
 * @file
 * Install, update, and uninstall functions for the SIPml5 module.
 */

/**
 * Implements hook_requirements().
 *
 * SIPml5 is a JavaScript implementation of Session Initiation Protocol
 * (SIP) from RFC 3261, a popular protocol for Internet-based
 * voice, video, chat, desktop sharing and other real-time protocols.
 *
 * Please download the SIPml5 JavaScript source file to the directory
 *
 *   $(DRUPAL_HOME)/libraries/sipml5/release/SIPml-api.js
 *
 * and manually create a VERSION file corresponding to the version
 * in use, for sipml5:
 *
 *   echo 1.0.0 > $(DRUPAL_HOME)/libraries/sipml5/VERSION
 *
 * The SIPml5 module is currently known to be used by version 7.x-1.x
 * of the DruCall module - see http://drucall.org
 */
function sipml5_requirements($phase) {

  $requirements = array();

  // Ensure translations do not break at install time.
  $t = get_t();

  $requirements['sipml5'] = array(
    'title' => $t('SIPml5 Library'),
  );

  $libraries = libraries_get_libraries();
  if (isset($libraries['sipml5'])) {
    $requirements['sipml5']['value'] = $t('Installed');
    $requirements['sipml5']['severity'] = REQUIREMENT_OK;
  }
  else {
    $requirements['sipml5']['value'] = $t('Not Installed');
    $requirements['sipml5']['severity'] = REQUIREMENT_ERROR;
    $requirements['sipml5']['description'] = $t('Please install the sipml5 library %url.', array('%url' => 'http://sipml5.org'));
  }

  return $requirements;
}
