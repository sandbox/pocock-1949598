<?php
/**
 * @file
 * Libraries API functions for the SIPml5 module.
 *
 * SIPml5 is a JavaScript implementation of Session Initiation Protocol
 * (SIP) from RFC 3261, a popular protocol for Internet-based
 * voice, video, chat, desktop sharing and other real-time protocols.
 */

/**
 * Implements hook_libraries_info().
 *
 * SIPml5 is a JavaScript implementation of Session Initiation Protocol
 * (SIP) from RFC 3261, a popular protocol for Internet-based
 * voice, video, chat, desktop sharing and other real-time protocols.
 *
 * Please download the SIPml5 JavaScript source file to the directory
 *
 *   $(DRUPAL_HOME)/libraries/sipml5/release/SIPml-api.js
 *
 * and manually create a VERSION file corresponding to the version
 * in use, for example:
 *
 *   echo 1.0.0 > $(DRUPAL_HOME)/libraries/sipml5/VERSION
 *
 * The SIPml5 module is currently known to be used by version 7.x-1.x
 * of the DruCall module - see http://drucall.org
 */
function sipml5_libraries_info() {
  $libraries['sipml5'] = array(
    'name' => 'SIPml5',
    'vendor url' => 'http://sipml5.org',
    'download url' => 'http://code.google.com/p/sipml5/wiki/Downloads?tm=2',
    'version arguments' => array(
      'file' => 'VERSION',
      'pattern' => '@([0-9\.]+)@',
      'lines' => 1,
      'cols' => 8,
    ),
    'files' => array(
      'js' => array(
        'release/SIPml-api.js',
      ),
    ),
  );
  return $libraries;
}
