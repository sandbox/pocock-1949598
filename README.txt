
Unpack the libraries API into your Drupal 7 modules directory.

Unpack this sipml5 module into your Drupal 7 modules directory.

Relative to the Drupal home,
  mkdir -p sites/all/libraries/sipml5/release

Download SIPml5-api.js from http://sipml5.org into
  sites/all/libraries/sipml5/release

echo "1.0" > sites/all/libraries/sipml5/VERSION

Now log in to Drupal, go to the Modules administration page and
enable the libraries and sipml5 modules.

Finally, install and enable any other modules that want to use SIPml5.
